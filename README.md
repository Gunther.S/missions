# README - Frontline Missions #

#### Making Missions ####
* Make sure there's a couple Main Base boxes (Empty section > Frontline). Having a couple is recommended so you don't need everyone to stand around the same box at the start of the game

#### Mission Guidelines ####
* Don't create zones that consist of a single completely walled off compound with very little entrances. We want people fighting in and over zones, not having a single guy aiming at the single entrance to your fortress the entire game. 
* Use existing roles from the mission, unless you have a clue about which changes you're making. Generally speaking roles should be somewhat similar between missions. We *dont* want an overload of presets, 500 different role and squad types and so on. 
* Don't create massive distances between zones. We want the player density to be fairly high and make sure there's a point in actually moving your way foward instead of only ever going full sprint mode to the next point.
* Pick areas with plenty of cover and different approaches. Altis is famous for having very open fields with one big hill overlooking the entire AO, having a single guy sitting on the hill mowing down everyone is only fun for a single players.
* Make missions that are fun for all the players. Mowing down 100 people in an Apache is fun for the pilot and gunner, but not fun for anyone on the other team. 
* You don't neccesarily need a bigger AO for higher player counts. Instead try experimenting with non-lineair layouts. 

#### Things to keep in mind ####
* Zone spawning is randomized on building positions or dense vegetation. Make sure some spawnable buildings are included. 
* The mod is very WIP, expect missions to break at random intervals. 

----------

### LICENSE ###

All missions are released under APL-SA ![](https://www.bistudio.com/assets/img/licenses/APL-SA.png)

Original PRA3 Misisons here: https://drakelinglabs.github.io/projectrealityarma3/

### General tips ###

* Install [PoseidonTools](https://forums.bistudio.com/topic/155514-poseidon-advanced-text-editor-for-scripts-configs/) as a good text editor for all your Arma needs
* Get [Shell Link Extension](http://schinagl.priv.at/nt/hardlinkshellext/linkshellextension.html) for easier symlink creation (Symlinks are great. Use them)
* Get [ShareX](https://getsharex.com/) for easier screenshot, file and whatever sharing. It's a better (and open source) alternative to the more popular Gyazo and lets you use whichever hosting service you want. 
* Fuck bitches, make money. 