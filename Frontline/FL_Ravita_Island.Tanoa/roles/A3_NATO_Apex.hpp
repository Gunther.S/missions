#define COMMON_ITEMS {"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}
#define COMMON_MEDICAL {"FRL_fieldDressing", 3}, {"FRL_Morphine", 1}, {"FRL_epinephrine", 2}

// NATO Pacific
class B_A3_NATO_Default {
	scope = 2;
	displayName = "str_a3_cfgvehicles_b_soldier_f0";
	abilities[] = {};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\rifleman_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};
	availableInGroups[] = {"Rifle", "Weapon"};
	requiredGroupMembers = -1;

	class Clothing {
		uniform   = "U_B_T_Soldier_F";
		headgear  = "H_HelmetB_tna_F";
		goggles   = "";
		vest      = "V_PlateCarrier2_tna_F";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_SPAR_01_khk_F";
				rail        = "acc_flashlight";
				optics      = "optic_Holosight_khk_F";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}};
			};

			class Pistol {
				weapon      = "hgun_P07_khk_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL};
		};

		class Variant2 : Variant1 {
			displayName = "CQB";

			class Primary {
				weapon      = "SMG_05_F";
				rail        = "";
				optics      = "optic_ACO_grn_smg";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_9x21_Mag_SMG_02", 9}};
			};
		};

		class Variant3 : Variant1 {
			displayName = "AR Support";

			class Backpack {
				backpack    = "B_AssaultPack_tna_F";
				content[] = {{"150Rnd_556x45_Drum_Mag_F", 6}};
			};
		};

		class Variant4 : Variant3 {
			displayName = "AT Support";

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 2}};
			};
		};
	};
};

class B_A3_NATO_SquadLeader : B_A3_NATO_Default {
	displayName = "str_b_soldier_sl_f0";
	abilities[] = {"RP", "Leader"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManOfficer_ca.paa";
	requiredGroupMembers = 0;

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr_khk_F";
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"itemGPS", 1}, {"Rangefinder", 1}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL};
		};
	};
};

class B_A3_NATO_TeamLeader_MG : B_A3_NATO_SquadLeader {
	displayName = "str_b_soldier_tl_f0";
	scope = 0;
	availableInGroups[] = {"MG"};
	abilities[] = {"Leader"};
	requiredGroupMembers = 1;

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AR Support";

			class Backpack {
				backpack    = "B_AssaultPack_tna_F";
				content[] = {{"150Rnd_556x45_Drum_Mag_F", 5}};
			};
		};
	};
};

class B_A3_NATO_TeamLeader_AA : B_A3_NATO_TeamLeader_MG {
	scope = 0;
	abilities[] = {"Leader"};
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AA Support";

			class Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class B_A3_NATO_TeamLeader_HAT : B_A3_NATO_TeamLeader_MG {
	scope = 0;
	abilities[] = {"Leader"};
	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "HAT Support";

			class Backpack {
				content[] = {{"Titan_AP", 1}, {"Titan_AT", 1}};
			};
		};
	};
};

class B_A3_NATO_Spotter : B_A3_NATO_SquadLeader {
	displayName = "str_b_spotter_f0";
	availableInGroups[] = {"Sniper"};
	requiredGroupMembers = 0;

	class Clothing {
		uniform   = "U_B_T_Sniper_F";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_oli";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
		};
	};
};

class B_A3_NATO_Medic : B_A3_NATO_Default {
	displayName = "str_b_medic_f0";
	abilities[] = {"Medic"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\medic_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMedic_ca.paa";
	requiredGroupMembers = 2;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack{
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_Morphine", 5}, {"FRL_epinephrine", 10}, {"SmokeShell", 6}};
			};
		};
	};
};

class B_A3_NATO_MG : B_A3_NATO_Default {
	displayName = "str_b_soldier_ar_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\autorifleman_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMG_ca.paa";
	availableInGroups[] = {"Rifle", "MG"};
	requiredGroupMembers = 3;

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "arifle_SPAR_02_khk_F";
				bipod       = "bipod_01_F_khk";
				magazines[] = {{"150Rnd_556x45_Drum_Mag_F", 5}};
			};
		};
	};
};

class B_A3_NATO_Grenadier : B_A3_NATO_Default {
	displayName = "str_b_soldier_gl_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\grenadier_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManExplosive_ca.paa";
	requiredGroupMembers = 4;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "arifle_SPAR_01_GL_khk_F";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
	};
};

class B_A3_NATO_LAT : B_A3_NATO_Default {
	displayName = "str_b_soldier_lat_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\LAT.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";
	requiredGroupMembers = 5;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_lat_f0";

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};

class B_A3_NATO_HAT : B_A3_NATO_Default {
	scope = 0;
	displayName = "str_b_soldier_at_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\HAT.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "launch_B_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AP", 1}};
			};
		};
	};
};

class B_A3_NATO_AA : B_A3_NATO_Default {
	scope = 0;
	displayName = "str_b_soldier_aa_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\AA.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "launch_B_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class B_A3_NATO_Marksman : B_A3_NATO_Default {
	displayName = "str_b_soldier_m_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\Marksman.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManRecon_ca.paa";
	requiredGroupMembers = 8;

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "arifle_SPAR_03_khk_F";
				optics      = "optic_DMS";
				bipod       = "bipod_01_F_khk";
				magazines[] = {{"20Rnd_762x51_Mag", 9}};
			};
		};
	};
};

class B_A3_NATO_Engineer : B_A3_NATO_Default {
	displayName = "str_b_engineer_f0";
	abilities[] = {"Engineer"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\engineer_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManEngineer_ca.paa";
	requiredGroupMembers = 6;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 2}, {"MineDetector", 1}};
			};
		};

		class Variant2 : Variant3 {
			displayName = "AP Minelayer";

			class Backpack : Backpack {
				content[]   = {{"APERSMine_Range_Mag", 1}, {"ClaymoreDirectionalMine_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};

		class Variant3 : Variant3 {
			displayName = "AT Minelayer";

			class Backpack : Backpack {
				content[]   = {{"ATMine_Range_Mag", 1}, {"ClaymoreDirectionalMine_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};
	};
};

class B_A3_NATO_Sniper : B_A3_NATO_Spotter {
	displayName = "str_b_sniper_f0";
	abilities[] = {};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sniper_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManRecon_ca.paa";
	availableInGroups[] = {"Sniper"};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "srifle_LRR_tna_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_LRPS_tna_F";
				bipod       = "";
				magazines[] = {{"7Rnd_408_Mag", 10}};
			};
		};
	};
};

class B_A3_NATO_Rifleman_Recon {
	displayName = "str_b_recon_f0";
	abilities[] = {};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\rifleman_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";
	availableInGroups[] = {"Recon"};
	requiredGroupMembers = 0;

	class Clothing {
		uniform   = "U_B_T_Soldier_AR_F";
		headgear  = "H_Booniehat_tna_F";
		goggles   = "";
		vest      = "V_PlateCarrier1_tna_F";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";
			class Primary {
				weapon      = "arifle_SPAR_01_khk_F";
				muzzle      = "muzzle_snds_m_khk_F";
				rail        = "acc_pointer_IR";
				optics      = "optic_ERCO_khk_F";
				bipod       = "";
				magazines[] = {{"30Rnd_556x45_Stanag", 9}};
			};

			class Pistol {
				weapon      = "hgun_P07_khk_F";
				muzzle      = "muzzle_snds_L";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"Binocular", 1}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL};
		};

		class Variant2 : Variant1 {
			displayName = "CQB";
			class Primary {
				weapon      = "SMG_05_F";
				muzzle      = "muzzle_snds_acp";
				rail        = "";
				optics      = "optic_ACO_grn_smg";
				bipod       = "";
				magazines[] = {{"30Rnd_9x21_Mag_SMG_02", 9}};
			};
		};
		class Variant3 : Variant1 {
			displayName = "AT Support";

			class Backpack {
				backpack    = "B_AssaultPack_tna_F";
				content[] = {{"NLAW_F", 2}};
			};
		};
	};
};

class B_A3_NATO_TeamLeader_Recon : B_A3_NATO_Rifleman_Recon {
	displayName = "str_b_recon_tl_f0";
	abilities[] = {"RP", "Leader"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManLeader_ca.paa";
	requiredGroupMembers = 0;

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"itemGPS", 1}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL, {"Rangefinder", 1}};
		};
	};
};

class B_A3_NATO_Medic_Recon : B_A3_NATO_Rifleman_Recon {
	displayName = "str_b_recon_medic_f0";
	abilities[] = {"Medic"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\medic_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMedic_ca.paa";
	requiredGroupMembers = 2;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_recon_medic_f0";

			class Backpack : Backpack{
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_Morphine", 5}, {"FRL_epinephrine", 10}, {"SmokeShell", 6}};
			};
		};
	};
};

class B_A3_NATO_Engineer_Recon : B_A3_NATO_Rifleman_Recon {
	displayName = "str_b_recon_exp_f0";
	abilities[] = {"Engineer"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\engineer_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManEngineer_ca.paa";
	requiredGroupMembers = 3;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"SatchelCharge_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};

		class Variant2 : Variant3 {
			displayName = "AP Minelayer";

			class Backpack : Backpack {
				content[]   = {{"APERSMine_Range_Mag", 1}, {"ClaymoreDirectionalMine_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};

		class Variant3 : Variant3 {
			displayName = "AT Minelayer";

			class Backpack : Backpack {
				backpack    = "B_AssaultPack_tna_F";
				content[]   = {{"ATMine_Range_Mag", 1}, {"ClaymoreDirectionalMine_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};
	};
};

class B_A3_NATO_LAT_Recon : B_A3_NATO_Rifleman_Recon {
	displayName = "str_b_recon_lat_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\LAT.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";
	requiredGroupMembers = 4;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_recon_lat_f0";

			class Secondary {
				weapon      = "launch_NLAW_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"NLAW_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"NLAW_F", 1}};
			};
		};
	};
};