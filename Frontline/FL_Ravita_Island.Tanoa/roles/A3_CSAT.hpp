#define COMMON_ITEMS {"ItemMap",1},{"ItemCompass",1},{"ItemWatch",1},{"ItemRadio",1}
#define COMMON_MEDICAL {"FRL_fieldDressing", 3}, {"FRL_Morphine", 1}, {"FRL_epinephrine", 2}

// CSAT Normal
class O_A3_CSAT_Default {
	scope = 2;
	displayName = "str_a3_cfgvehicles_b_soldier_f0";
	abilities[] = {};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\rifleman_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";
	compassIcon[] = {"a3\ui_f\data\map\Markers\Military\dot_ca.paa", 3.6};
	availableInGroups[] = {"Rifle", "Weapon"};
	requiredGroupMembers = -1;

	class Clothing {
		uniform   = "U_O_CombatUniform_ocamo";
		headgear  = "H_HelmetO_ocamo";
		goggles   = "";
		vest      = "V_HarnessO_brn";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";

			class Primary {
				weapon      = "arifle_Katiba_F";
				rail        = "acc_pointer_IR";
				optics      = "optic_Aco";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_green", 9}};
			};

			class Pistol {
				weapon      = "hgun_Rook40_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL};
		};

		class Variant2 : Variant1 {
			displayName = "CQB";

			class Primary {
				weapon      = "SMG_02_F";
				rail        = "";
				optics      = "optic_ACO_smg";
				muzzle      = "";
				bipod       = "";
				magazines[] = {{"30Rnd_9x21_Mag", 9}};
			};
		};

		class Variant3 : Variant1 {
			displayName = "AR Support";

			class Backpack {
				backpack    = "B_FieldPack_ocamo";
				content[] = {{"150Rnd_762x54_Box", 4}};
			};
		};

		class Variant4 : Variant3 {
			displayName = "AT Support";

			class Backpack : Backpack {
				content[] = {{"RPG32_F", 2}};
			};
		};
	};
};

class O_A3_CSAT_SquadLeader : O_A3_CSAT_Default {
	displayName = "str_b_soldier_sl_f0";
	abilities[] = {"RP", "Leader"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManOfficer_ca.paa";
	requiredGroupMembers = 0;

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"itemGPS", 1}, {"Rangefinder", 1}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL};
		};
	};
};

class O_A3_CSAT_TeamLeader_MG : O_A3_CSAT_SquadLeader {
	displayName = "str_b_soldier_tl_f0";

	scope = 0;
	availableInGroups[] = {"MG"};
	abilities[] = {"Leader"};
	requiredGroupMembers = 1;

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AR Support";

			class Backpack {
				backpack    = "B_FieldPack_ocamo";
				content[] = {{"150Rnd_762x54_Box", 4}};
			};
		};
	};
};

class O_A3_CSAT_TeamLeader_AA : O_A3_CSAT_TeamLeader_MG {
	scope = 0;
	abilities[] = {"Leader"};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "AA Support";

			class Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class O_A3_CSAT_TeamLeader_HAT : O_A3_CSAT_TeamLeader_MG {
	scope = 0;
	abilities[] = {"Leader"};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "HAT Support";

			class Backpack {
				content[] = {{"Titan_AP", 1}, {"Titan_AT", 1}};
			};
		};
	};
};

class O_A3_CSAT_Spotter : O_A3_CSAT_SquadLeader {
	displayName = "str_b_spotter_f0";
	availableInGroups[] = {"Sniper"};
	requiredGroupMembers = 0;

	class Clothing {
		uniform   = "U_O_GhillieSuit";
		headgear  = "";
		goggles   = "";
		vest      = "V_Chestrig_khk";
	};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_spotter_f0";
		};
	};
};

class O_A3_CSAT_Medic : O_A3_CSAT_Default {
	displayName = "str_b_medic_f0";
	abilities[] = {"Medic"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\medic_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMedic_ca.paa";
	requiredGroupMembers = 2;


	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_medic_f0";

			class Backpack : Backpack{
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_Morphine", 5}, {"FRL_epinephrine", 10}, {"SmokeShell", 6}};
			};
		};
	};
};

class O_A3_CSAT_MG : O_A3_CSAT_Default {
	displayName = "str_b_soldier_ar_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\autorifleman_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMG_ca.paa";
	availableInGroups[] = {"Rifle", "MG"};
	requiredGroupMembers = 3;

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_ar_f0";

			class Primary : Primary {
				weapon      = "LMG_Zafir_F";
				magazines[] = {{"150Rnd_762x54_Box", 4}};
			};
		};
	};
};

class O_A3_CSAT_Grenadier : O_A3_CSAT_Default {
	displayName = "str_b_soldier_gl_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\grenadier_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManExplosive_ca.paa";
	requiredGroupMembers = 4;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_gl_f0";

			class Primary : Primary {
				weapon      = "arifle_Katiba_GL_F";
				magazines[] = {{"30Rnd_65x39_caseless_green", 9}, {"1Rnd_HE_Grenade_shell", 8}};
			};

			class Backpack : Backpack{
				content[] = {{"1Rnd_Smoke_Grenade_shell", 4}, {"UGL_FlareWhite_F", 4}};
			};
		};
	};
};

class O_A3_CSAT_LAT : O_A3_CSAT_Default {
	displayName = "str_b_soldier_lat_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\LAT.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";
	requiredGroupMembers = 5;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_lat_f0";

			class Secondary {
				weapon      = "launch_RPG32_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"RPG32_F", 1}};
			};
		};
	};
};

class O_A3_CSAT_HAT : O_A3_CSAT_Default {
	scope = 0;
	displayName = "str_b_soldier_at_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\HAT.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_at_f0";

			class Secondary {
				weapon      = "launch_O_Titan_short_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AT", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AP", 1}};
			};
		};
	};
};

class O_A3_CSAT_AA : O_A3_CSAT_Default {
	scope = 0;
	displayName = "str_b_soldier_aa_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\AA.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_soldier_aa_f0";

			class Secondary {
				weapon      = "launch_O_Titan_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"Titan_AA", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"Titan_AA", 1}};
			};
		};
	};
};

class O_A3_CSAT_Marksman : O_A3_CSAT_Default {
	displayName = "str_b_soldier_m_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\Marksman.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManRecon_ca.paa";
	requiredGroupMembers = 8;

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_soldier_m_f0";

			class Primary : Primary {
				weapon      = "srifle_DMR_01_F";
				optics      = "optic_DMS";
				bipod       = "bipod_02_F_hex";
				magazines[] = {{"10Rnd_762x54_Mag", 9}};
			};
		};
	};
};

class O_A3_CSAT_Engineer : O_A3_CSAT_Default {
	displayName = "str_b_engineer_f0";
	abilities[] = {"Engineer"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\engineer_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManEngineer_ca.paa";
	requiredGroupMembers = 6;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"FRL_BreachingCharge_Wpn", 2}, {"MineDetector", 1}};
			};
		};

		class Variant2 : Variant3 {
			displayName = "AP Minelayer";

			class Backpack : Backpack {
				content[]   = {{"APERSMine_Range_Mag", 1}, {"ClaymoreDirectionalMine_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};

		class Variant3 : Variant3 {
			displayName = "AT Minelayer";

			class Backpack : Backpack {
				content[]   = {{"ATMine_Range_Mag", 1}, {"ClaymoreDirectionalMine_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};
	};
};

class O_A3_CSAT_Sniper : O_A3_CSAT_Spotter {
	displayName = "str_b_sniper_f0";
	abilities[] = {};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sniper_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManRecon_ca.paa";
	availableInGroups[] = {"Sniper"};

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "str_b_sniper_f0";
			class Primary {
				weapon      = "srifle_GM6_camo_F";
				muzzle      = "";
				rail        = "";
				optics      = "optic_LRPS";
				bipod       = "";
				magazines[] = {{"5Rnd_127x108_Mag", 10}};
			};
		};
	};
};

class O_A3_CSAT_Rifleman_Recon {
	displayName = "str_b_recon_f0";
	abilities[] = {};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\rifleman_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconMan_ca.paa";
	availableInGroups[] = {"Recon"};
	requiredGroupMembers = 0;

	class Clothing {
		uniform   = "U_O_SpecopsUniform_ocamo";
		headgear  = "H_HelmetSpecO_ocamo";
		goggles   = "G_Bandanna_khk";
		vest      = "V_HarnessO_brn";
	};

	class Variants {
		class Variant1 {
			displayName = "Standard";
			class Primary {
				weapon      = "arifle_Katiba_F";
				muzzle      = "muzzle_snds_H";
				rail        = "acc_pointer_IR";
				optics      = "optic_MRCO";
				bipod       = "";
				magazines[] = {{"30Rnd_65x39_caseless_green", 9}};
			};

			class Pistol {
				weapon      = "hgun_Rook40_F";
				muzzle      = "muzzle_snds_L";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"16Rnd_9x21_Mag", 3}};
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"Binocular", 1}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL};
		};

		class Variant2 : Variant1 {
			displayName = "CQB";
			class Primary {
				weapon      = "SMG_02_F";
				muzzle      = "muzzle_snds_acp";
				rail        = "";
				optics      = "optic_Aco_smg";
				bipod       = "";
				magazines[] = {{"30Rnd_9x21_Mag", 9}};
			};
		};
		class Variant3 : Variant1 {
			displayName = "AT Support";

			class Backpack {
				backpack    = "B_FieldPack_ocamo";
				content[] = {{"RPG32_F", 2}};
			};
		};
	};
};

class O_A3_CSAT_TeamLeader_Recon : O_A3_CSAT_Rifleman_Recon {
	displayName = "str_b_recon_tl_f0";
	abilities[] = {"RP", "Leader"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\sqleader_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManLeader_ca.paa";
	requiredGroupMembers = 0;

	class Variants : Variants {
		class Variant1 : Variant1 {
			displayName = "Standard";

			class Primary : Primary {
				optics      = "optic_Hamr";
			};

			items[]       = {{"SmokeShell", 2}, {"HandGrenade", 2}, {"itemGPS", 1}};
			itemshidden[] = {COMMON_ITEMS, COMMON_MEDICAL, {"Rangefinder", 1}};
		};
	};
};

class O_A3_CSAT_Medic_Recon : O_A3_CSAT_Rifleman_Recon {
	displayName = "str_b_recon_medic_f0";
	abilities[] = {"Medic"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\medic_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManMedic_ca.paa";
	requiredGroupMembers = 2;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_recon_medic_f0";

			class Backpack : Backpack {
				content[] = {{"FRL_fieldDressing", 20}, {"FRL_Morphine", 5}, {"FRL_epinephrine", 10}, {"SmokeShell", 6}};
			};
		};
	};
};

class O_A3_CSAT_Engineer_Recon : O_A3_CSAT_Rifleman_Recon {
	displayName = "str_b_recon_exp_f0";
	abilities[] = {"Engineer"};
	UIIcon = "\pr\frl\addons\client\ui\media\roles\engineer_small_88.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManEngineer_ca.paa";
	requiredGroupMembers = 3;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "Demolition Specialist";

			class Backpack : Backpack {
				content[]   = {{"DemoCharge_Remote_Mag", 2}, {"SatchelCharge_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};

		class Variant2 : Variant3 {
			displayName = "AP Minelayer";

			class Backpack : Backpack {
				content[]   = {{"APERSMine_Range_Mag", 1}, {"ClaymoreDirectionalMine_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};

		class Variant3 : Variant3 {
			displayName = "AT Minelayer";

			class Backpack : Backpack {
				backpack    = "B_AssaultPack_rgr";
				content[]   = {{"ATMine_Range_Mag", 1}, {"ClaymoreDirectionalMine_Remote_Mag", 1}, {"MineDetector", 1}};
			};
		};
	};
};

class O_A3_CSAT_LAT_Recon : O_A3_CSAT_Rifleman_Recon {
	displayName = "str_b_recon_lat_f0";
	UIIcon = "\pr\frl\addons\client\ui\media\roles\LAT.paa";
	mapIcon = "\A3\ui_f\data\map\vehicleicons\IconManAT_ca.paa";
	requiredGroupMembers = 4;

	class Variants : Variants {
		class Variant1 : Variant3 {
			displayName = "str_b_recon_lat_f0";

			class Secondary {
				weapon      = "launch_RPG32_F";
				muzzle      = "";
				rail        = "";
				optics      = "";
				bipod       = "";
				magazines[] = {{"RPG32_F", 1}};
			};

			class Backpack : Backpack {
				content[] = {{"RPG32_F", 1}};
			};
		};
	};
};