class GroupTypes {
    class Rifle {
        displayName = "Rifle";
        groupSize = 9;
        requiredGroups = 0;
        requiredPlayers = 1;
        mapIcon = "\A3\ui_f\data\map\markers\nato\b_inf.paa";
    };

    class Recon: Rifle {
        displayName = "Recon";
        groupSize = 5;
        requiredGroups = 1;
        requiredPlayers = 20;
        mapIcon = "\A3\ui_f\data\map\markers\nato\b_inf.paa";
    };

    class Sniper: Rifle {
        displayName = "Sniper";
        groupSize = 2;
        requiredGroups = 3;
        requiredPlayers = 25;
        mapIcon = "\A3\ui_f\data\map\markers\nato\b_inf.paa";
    };

    class MG: Rifle {
        groupSize = 3;
        requiredGroups = 1;
        requiredPlayers = 10;
        mapIcon = "\A3\ui_f\data\map\markers\nato\b_inf.paa";
    };
};
