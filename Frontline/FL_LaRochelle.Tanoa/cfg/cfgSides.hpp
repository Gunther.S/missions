class Sides {
    class West {
        name = "NATO";
        playerClass = "B_Soldier_F";
        //flag = "a3\data_f\Flags\flag_nato_co.paa";
        flag = "a3\Data_f\cfgFactionClasses_BLU_ca.paa";
        mapIcon = "a3\ui_f\data\Map\Markers\NATO\b_installation.paa";
        color[] = {0, 0.3, 0.8, 1};
        squadRallyPointObject = "ADT_Backpacks_West";
        FOBObjects[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};

        class Kits {
            #include "..\roles\A3_NATO_Apex.hpp"
        };

        #include "cfgLogisticWest.hpp"
    };

    class East : West {
        name = "PRC";
        playerClass = "O_Soldier_F";
        //flag = "a3\data_f\Flags\flag_csat_co.paa";
        flag = "pr\frl\addons\uniforms\factions\Cfgfactionclasses_chn_pla.paa";
        mapIcon = "a3\ui_f\data\Map\Markers\NATO\o_installation.paa";
        color[] = {0.5, 0, 0, 1};
        squadRallyPointObject = "ADT_Backpacks_East";
        FOBObjects[] = {{"FRL_FO_Box1", {0,0,0}, 0}, {"Land_SatelliteAntenna_01_F", {0,0.22,0}, 177}};

        class Kits {
            #include "..\roles\A3_CSAT_Apex.hpp"
        };

        #include "cfgLogisticEast.hpp"

    };
};
