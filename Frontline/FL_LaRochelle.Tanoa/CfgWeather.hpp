class CfgWeather {
	overcastRandomize = 1;
	overcastFrame[] = {0.2,0.7}; // [0,1]

	rainRandomize = 0;
	rainFrame[] = {0,1}; // [0,1]

	fogRandomize = 1;
	fogFrame[] = {0,0.3}; // [0,1]

	timeRandomize = 1;
	timeFrame[] = {6.5,14}; // [0,24]
};