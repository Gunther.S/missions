class sector {
    bleedValue[] = {15,15}; // Frontline
    captureTime[] = {90,180};
    firstCaptureTime[] = {60,120};
    sectortype = "normal";

    dependency[] = {};
    ticketValue = 0;
    minUnits = 1;
    isLastSector = "";
};

class CfgSectors {

    class base_west : sector {
        designator = "HQ";
        sectortype = "mainbase";
        bleedValue = 0;
    };

    class base_east : sector {
        designator = "HQ";
        sectortype = "mainbase";
        bleedValue = 0;
    };

    class CfgSectorPath {
        class path_0 {
            class sector_0 : sector {
                dependency[] = {"base_west","sector_1"};
                designator = "A";
            };

            class sector_1 : sector {
                dependency[] = {"sector_0","sector_2"};
                designator = "B";
            };

            class sector_2 : sector {
                dependency[] = {"sector_1","sector_3"};
                designator = "C";
            };

            class sector_3 : sector {
                dependency[] = {"sector_2","sector_4"};
                designator = "D";
            };

			class sector_4 : sector {
                dependency[] = {"sector_3","sector_5"};
                designator = "E";
            };

			class sector_5 : sector {
                dependency[] = {"sector_4","sector_6"};
                designator = "F";
            };

			class sector_6 : sector {
                dependency[] = {"sector_5","base_east"};
                designator = "G";
            };
        };

        class path_1 {
            class sector_7 : sector {
                dependency[] = {"base_west","sector_8"};
                designator = "A";
            };

            class sector_8 : sector {
                dependency[] = {"sector_7","sector_9"};
                designator = "B";
            };

            class sector_9 : sector {
                dependency[] = {"sector_8","sector_10"};
                designator = "C";
            };

            class sector_10 : sector {
                dependency[] = {"sector_9","sector_11"};
                designator = "D";
            };

			class sector_11 : sector {
                dependency[] = {"sector_10","sector_12"};
                designator = "E";
            };

			class sector_12 : sector {
                dependency[] = {"sector_11","sector_13"};
                designator = "F";
            };

			class sector_13 : sector {
                dependency[] = {"sector_12","base_east"};
                designator = "G";
            };
        };

        class path_3 {
            class sector_14 : sector {
                dependency[] = {"base_west","sector_15"};
                designator = "A";
            };

            class sector_15 : sector {
                dependency[] = {"sector_14","sector_16"};
                designator = "B";
            };

            class sector_16 : sector {
                dependency[] = {"sector_15","sector_17"};
                designator = "C";
            };

            class sector_17 : sector {
                dependency[] = {"sector_16","sector_18"};
                designator = "D";
            };

			class sector_18 : sector {
                dependency[] = {"sector_17","sector_19"};
                designator = "E";
            };

			class sector_19 : sector {
                dependency[] = {"sector_18","sector_20"};
                designator = "F";
            };

			class sector_20 : sector {
                dependency[] = {"sector_19","base_east"};
                designator = "G";
            };
        };

        class path_4 {
            class sector_21 : sector {
                dependency[] = {"base_west","sector_22"};
                designator = "A";
            };

            class sector_22 : sector {
                dependency[] = {"sector_21","sector_23"};
                designator = "B";
            };

            class sector_23 : sector {
                dependency[] = {"sector_22","sector_24"};
                designator = "C";
            };

            class sector_24 : sector {
                dependency[] = {"sector_23","sector_25"};
                designator = "D";
            };

			class sector_25 : sector {
                dependency[] = {"sector_24","sector_26"};
                designator = "E";
            };

			class sector_26 : sector {
                dependency[] = {"sector_25","sector_27"};
                designator = "F";
            };

			class sector_27 : sector {
                dependency[] = {"sector_26","base_east"};
                designator = "G";
            };
        };

        class path_5 {
            class sector_28 : sector {
                dependency[] = {"base_west","sector_29"};
                designator = "A";
            };

            class sector_29 : sector {
                dependency[] = {"sector_28","sector_30"};
                designator = "B";
            };

            class sector_30 : sector {
                dependency[] = {"sector_29","sector_31"};
                designator = "C";
            };

            class sector_31 : sector {
                dependency[] = {"sector_30","sector_32"};
                designator = "D";
            };

			class sector_32 : sector {
                dependency[] = {"sector_31","sector_33"};
                designator = "E";
            };

			class sector_33 : sector {
                dependency[] = {"sector_32","sector_34"};
                designator = "F";
            };

			class sector_34 : sector {
                dependency[] = {"sector_33","base_east"};
                designator = "G";
            };
        };

        class path_6 {
            class sector_35 : sector {
                dependency[] = {"base_west","sector_36"};
                designator = "A";
            };

            class sector_36 : sector {
                dependency[] = {"sector_35","sector_37"};
                designator = "B";
            };

            class sector_37 : sector {
                dependency[] = {"sector_36","sector_38"};
                designator = "C";
            };

            class sector_38 : sector {
                dependency[] = {"sector_37","sector_39"};
                designator = "D";
            };

			class sector_39 : sector {
                dependency[] = {"sector_38","sector_40"};
                designator = "E";
            };

			class sector_40 : sector {
                dependency[] = {"sector_39","sector_41"};
                designator = "F";
            };

			class sector_41 : sector {
                dependency[] = {"sector_40","base_east"};
                designator = "G";
            };
        };
    };
};
