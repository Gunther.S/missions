class cfgVoting {
	class switchMission {
		scope = 2;
		showToUser = 1;
		condition = "(missionNamespace getVariable ['frl_frontline_FrontlineInitDone', false])";
		requiredRatio = 0.65;
		displayName = "Switch Mission";
		voteYesText = "The current mission will end in 10s.";
		voteNoText = "The current mission will continue.";
		voteNoCode = "true";
		questionText = "Do you want to end the current mission?";
		voteTarget = "all";
		voteYesCode = "[{ ['durationEnd', [time]] call FRL_main_fnc_persistentEventGlobal }, 10] call PRA3_core_fnc_wait;";
	};

	class pauseBattleprep: switchMission {
		scope = 0;
		condition = "true";
		displayName = "Pause battleprep";
		voteYesText = "Battleprep will be paused.";
		voteNoText = "Battleprep won't be paused.";
		questionText = "Do you want to pause battleprep?";
		voteYesCode = "[true] call FRL_prep_fnc_setPausedPrep;";
	};

	class continueBattleprep: pauseBattleprep {
		condition = "false";
		displayName = "Continue battleprep";
		voteYesText = "Battleprep will be continued.";
		voteNoText = "Battleprep won't be continued.";
		questionText = "Do you want to resume battleprep?";
		voteYesCode = "[false] call FRL_prep_fnc_setPausedPrep;";
	};

	class extendTime: switchMission {
		condition = "([false] call FRL_duration_fnc_getMissionDuration) >= 90;";
		displayName = "Extend duration (30m)";
		voteYesText = "The mission duration will be extended 30 minutes.";
		voteYesCode = "[false] call FRL_prep_fnc_setPausedPrep;";
		voteNoText = "The mission duration will remain the same.";
		questionText = "Do you want to extend mission duration 30 minutes?";
	};

	class reduceTime: extendTime {
		condition = "([true] call FRL_duration_fnc_getMissionDuration) >= 1890;";
		displayName = "Reduce duration (30m)";
		voteYesText = "The mission duration will be reduced 30 minutes.";
		voteYesCode = "[false] call FRL_prep_fnc_setPausedPrep;";
		questionText = "Do you want to reduce mission duration 30 minutes?";
	};

	class raiseTickets: switchMission {
		condition = "true";
		displayName = "Increase tickets (30)";
		voteYesText = "Tickets for both sides increased by 30.";
		voteYesCode = "[30] call FRL_voting_fnc_adjustTickets;";
		voteNoText = "Tickets will remain unchanged.";
		questionText = "Do you want to increase tickets by 30?";
	};

	class lowerTickets: raiseTickets {
		displayName = "Reduce tickets (30)";
		voteYesText = "Tickets for both sides lowered by 30.";
		voteYesCode = "[-30] call FRL_voting_fnc_adjustTickets;";
		questionText = "Do you want to lower tickets by 30?";
	};

	class surrenderVote: switchMission {
		voteTarget = "callerside";
		displayName = "Surrender";
		questionText = "Do you want to surrender and end the current mission?";
	};

};