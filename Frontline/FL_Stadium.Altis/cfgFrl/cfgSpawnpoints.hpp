class CfgZoneSpawning {
    enabled = 1;
    maxEnemyCount = 1;
    maxEnemyCountRadius = 50;
};

class cfgSquadRallyPoint {
    //minDistance = 0;
    minDistanceEnemyZone = 100;
    //spawnCount = 9;
    //nearPlayerToBuild = -1;
    //nearPlayerToBuildRadius = -1;
    maxEnemyCount = 2;
    maxEnemyCountRadius = 40;
    uptime = 90;
    waitTime = 150;
    rearmCooldown = 300; //600;
};

class cfgFOB {
    minDistanceEnemyZone = 200;
    maxDistanceFriendlyZone = 400;
    enemyBlockCount = 2;
    enemyBlockRadius = 75;
    blockDuration = 100;
    nearPlayerToBuild = 3;
    nearPlayerToBuildRadius = 15;
    spacingDistance = 400;
};
