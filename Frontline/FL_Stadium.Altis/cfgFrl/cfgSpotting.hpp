class cfgSpotting {
	class DefaultSpot {
		scope = 2;
		markerColor[] = {1, 1, 1, 0.8};
		displayName = "Unknown";
		displayNameShort = "INF";
		markerPath = "\pr\frl\addons\client\markers\opfor\frl_o_unknown.paa";
		listPath = "\pr\frl\addons\client\markers\opfor\frl_o_unknown.paa";
	};

	class Spotting {
		category = 1;
		displayName = "SPOT";

		class Infantry: DefaultSpot {
			markerColor[] = {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
			displayName = "Infantry";
			displayNameShort = "INF";
			markerPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf.paa";
			listPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf.paa";
		};

		class AntiTank: Infantry {
			displayName = "Anti-Tank";
			displayNameShort = "AT";
			markerPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf_atk.paa";
			listPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf_atk.paa";
		};

		class AntiAir: Infantry {
			displayName = "Anti-Air";
			displayNameShort = "AA";
			markerPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf_aa.paa";
			listPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf_aa.paa";
		};

		class Sniper: Infantry {
			displayName = "Sniper";
			displayNameShort = "SNP";
			markerPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf_snpr.paa";
			listPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf_snpr.paa";
		};

		class UnarmedVehicle: Infantry {
			displayName = "Unarmed Vehicle";
			displayNameShort = "VHC";
			markerPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf_motorised.paa";
			listPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf_motorised.paa";
		};

		class ArmedVehicle: Infantry {
			displayName = "Armed Vehicle";
			displayNameShort = "Armd. VHC";
			markerPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf_motorised_wpn.paa";
			listPath = "\pr\frl\addons\client\markers\opfor\frl_o_inf_motorised_wpn.paa";
		};

		class ArmoredVehicle: Infantry {
			displayName = "Armored Vehicle";
			displayNameShort = "AMR";
			markerPath = "\pr\frl\addons\client\markers\opfor\frl_o_armd.paa";
			listPath = "\pr\frl\addons\client\markers\opfor\frl_o_armd.paa";
		};

		class StaticWeapon: Infantry {
			displayName = "Static Weapon";
			displayNameShort = "HMG";
			markerPath = "\pr\frl\addons\client\markers\opfor\frl_o_hmg.paa";
			listPath = "\pr\frl\addons\client\markers\opfor\frl_o_hmg.paa";
		};

		class Outpost: Infantry {
			displayName = "Foward Outpost";
			displayNameShort = "FO";
			markerPath = "\pr\frl\addons\client\markers\opfor\frl_o_hq.paa";
			listPath = "\pr\frl\addons\client\markers\opfor\frl_o_hq.paa";
		};
	};

	class Request {
		category = 1;
		displayName = "REQUEST";

		class Medic: DefaultSpot {
			displayName = "Medic";
			markerColor[] = {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
			displayNameShort = "MED";
			markerPath = "\pr\frl\addons\client\markers\blufor\frl_b_med.paa";
			listPath = "\pr\frl\addons\client\markers\blufor\frl_b_med.paa";
		};

		class Defenses: Medic {
			displayName = "Build Defenses";
			displayNameShort = "DEF";
			markerPath = "\pr\frl\addons\client\markers\blufor\frl_b_labor.paa";
			listPath = "\pr\frl\addons\client\markers\blufor\frl_b_labor.paa";
		};

		class Outpost: Medic {
			displayName = "Deploy FO";
			displayNameShort = "FO";
			markerPath = "\pr\frl\addons\client\markers\blufor\frl_b_hq.paa";
			listPath = "\pr\frl\addons\client\markers\blufor\frl_b_hq.paa";
		};
	};

};