class CfgMedical {
    bleedoutTimeSteps[] = {210,150,0};
    bleedoutTimeReset = 300;
    maxDamageOnLegsBeforWalking = 0.7;

    treatmentTimeAdrenalineMedic = 6;
    treatmentTimeAdrenaline = 10;

    treatmentTimeMorphineMedic = 6;
    treatmentTimeMorphine = 10;

    healthIncreaseMedic = 0.5;
    healthIncrease = 0.25;
    healthIncreaseCap = 0.25;

    treatmentTimeBandage = 10;
    treatmentTimeBandageMedic = 6;

    treatmentTimeCPR = 10;
    treatmentTimeCPRMedic = 10;
    treatmentTickCPR = 1;
    treatmentEffectCPR = 8;
    treatmentEffectCPRMedic = 13;

};
