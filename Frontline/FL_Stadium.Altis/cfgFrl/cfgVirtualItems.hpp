    class VirtualItems {
    	class FieldDressing {
            scope = 2;
            model = "A3\Structures_F_EPA\Items\Medical\Bandage_F.p3d";
            itemRequired = "FRL_fieldDressing";
    		attachPoint = "lwrist";
    		attachOffset[] = {0.05, 0.10, -0.01};
    		vectorDir[] = {{1, 0, 0}, {0, 0, 1}};
            actionVerb = "bandage";
            actionProgressText = "Bandaging %1 ...";
            actionName = "Field Dressing";
            allowInVehicle = 0;
            animUsageStand = "AinvPknlMstpSlayWnonDnon_medicOther";
            animUsageCrouch = "AinvPknlMstpSlayWnonDnon_medicOther";
            animUsageProne = "AinvPpneMstpSlayWnonDnon_medicOther";
    	};

        class Morphine: FieldDressing {
            model = "A3\weapons_F\ammo\mag_univ.p3d";
            itemRequired = "FRL_morphine";
            attachPoint = "lwrist";
            attachOffset[] = {-0.02, 0, -0.13};
            vectorDir[] = {{0, 1, 0}, {1, 0, 0}};
            actionVerb = "inject";
            actionProgressText = "Injecting %1 ...";
            actionName = "Morphine";
        };

        class Adrenaline: Morphine {
            itemRequired = "FRL_epinephrine";
            actionName = "Adrenaline";
        };
    };