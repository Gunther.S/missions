enableSaving [false, false];
enableSentences false;

waitUntil {!isNil "PRA3_Core_fnc_loadModules"};

(getArray (missionConfigFile >> "PRA3" >> "loadModules")) call PRA3_Core_fnc_loadModules;
